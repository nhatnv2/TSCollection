//
//  UIView+TSImage.swift
//  TSCollection
//
//  Created by sanghv on 10/19/16.
//  Copyright © 2016 TS. All rights reserved.
//

import Foundation

/** TSImage Extends UIView

*/
public extension UIView {
    
    func imageBySize(size: CGSize, scale: CGFloat = UIScreen.main.scale) -> UIImage {
        let selector = #selector(setter: CALayer.shouldRasterize)

        let respond = self.layer.responds(to: selector)
        if respond {
            UIGraphicsBeginImageContextWithOptions(size, false, self.contentScaleFactor * scale)
        } else {
            UIGraphicsBeginImageContext(size)
        }

        self.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return image!
    }
}
