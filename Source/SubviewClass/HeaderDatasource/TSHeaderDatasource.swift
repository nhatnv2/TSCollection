//
//  TSHeaderDatasource.swift
//  TSCollection
//
//  Created by sanghv on 10/19/16.
//  Copyright © 2016 TS. All rights reserved.
//

import Foundation

/** TSHeaderDatasource protocol

*/
public protocol TSHeaderDatasource {

    static var headerIdentifier: String { get }
    func configHeaderWithData(data: Any?)
}
