//
//  TSBaseTableViewHeaderFooterView.swift
//  TSCollection
//
//  Created by sanghv on 10/19/16.
//  Copyright © 2016 TS. All rights reserved.
//

import UIKit

open class TSBaseTableViewHeaderFooterView: UITableViewHeaderFooterView {

    deinit {

    }

    open weak var delegate: AnyObject?
    open var section: Int?

    override public init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)

        self.configView()
    }

    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override open func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

        self.configView()
    }

    open func setSection(section: Int?, sender: AnyObject?) {
        self.section = section
        self.delegate = sender
    }
}

@objc extension TSBaseTableViewHeaderFooterView {

    open func configView() {

    }
}

@objc public extension TSBaseTableViewHeaderFooterView {

    // MARK: - Reuse identifer

    class var identifier: String {
        get {
            let mirror = Mirror(reflecting: self)
            return "\(String(describing: mirror.subjectType).replacingOccurrences(of: ".Type", with: ""))ID"
        }
    }
}

@objc extension TSBaseTableViewHeaderFooterView: TSHeaderDatasource {

    // MARK: - TSHeaderDatasource

    open class var headerIdentifier: String {
        get {
            return "HeaderIdentifier"
        }
    }

    open func configHeaderWithData(data: Any?) {
        
    }
}
